from types import ModuleType
from pytest import fixture

from budosystems.xtra import pylint_plugin

def test_subproject_import():
    assert isinstance(pylint_plugin, ModuleType)
